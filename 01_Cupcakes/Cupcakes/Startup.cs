﻿using Cupcakes.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Cupcakes.Repositories;

namespace Cupcakes
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICupcakeRepository, CupcakeRepository>();

            services.AddControllersWithViews();
            services.AddDbContext<CupcakeContext>(options => options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection")));
        }

        public void Configure(IApplicationBuilder app)
        {    
            app.UseStaticFiles();
            app.UseRouting();

            app.UseEndpoints(points=>
            {
                points.MapControllerRoute(
                    name: "CupcakeRoute",
                    pattern: "{controller=Cupcake}/{action=Index}/{id?}");
            });
        }
    }
}